import { Vector3 } from "babylonjs";
var moveForward = false;
var moveBackward = false;
var moveRight = false;
var moveLeft = false;

function onKeyDown(event) {
    switch (event.keyCode) {
        case 38: // up
        case 87: // w
            moveForward = true;
            break;

        case 37: // left
        case 65: // a
            moveLeft = true; break;

        case 40: // down
        case 83: // s
            moveBackward = true;
            break;

        case 39: // right
        case 68: // d
            moveRight = true;
            break;

        case 32: // space
            break;
    }
}

function onKeyUp (event) {
    switch (event.keyCode) {
        case 38: // up
        case 87: // w
            moveForward = false;
            break;

        case 37: // left
        case 65: // a
            moveLeft = false;
            break;

        case 40: // down
        case 83: // a
            moveBackward = false;
            break;

        case 39: // right
        case 68: // d
            moveRight = false;
            break;
    }
}
function setWalkingBehavior(hero, camera) {
    camera.position.x = hero.position.x;
    camera.position.y = hero.position.y + 1.0;
    camera.position.z = hero.position.z;

    var forward = camera.getTarget().subtract(camera.position).normalize();
    forward.y = 0;
    var right = Vector3.Cross(forward, camera.upVector).normalize();
    right.y = 0;

    var SPEED = 20;
    let f_speed = 0;
    var s_speed = 0;
    var u_speed = 0;

    if (moveForward) {
        f_speed = SPEED;
    }
    if (moveBackward) {
        f_speed = -SPEED;
    }

    if (moveRight) {
        s_speed = SPEED;
    }

    if (moveLeft) {
        s_speed = -SPEED;
    }

    var move = (forward.scale(f_speed)).subtract((right.scale(s_speed))).subtract(camera.upVector.scale(u_speed));

    hero.physicsImpostor.physicsBody.velocity.x = move.x;
    hero.physicsImpostor.physicsBody.velocity.z = move.z;
    hero.physicsImpostor.physicsBody.velocity.y = move.y;

}
export default {
    onKeyDown,
    onKeyUp,
    setWalkingBehavior
}