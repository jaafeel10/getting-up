import {
  Scene,
  Engine,
  FreeCamera,
  Vector3,
  HemisphericLight,
  // SceneLoader,
  // AssetsManager,
  MeshBuilder,
  StandardMaterial,
  Color3,
  Mesh,
  Ray,
  RayHelper,
  CreateBox,
  Texture,
  CreateGroundFromHeightMap,
  GroundMesh,
  // PhysicsImpostor,
  AssetsManager,
  CannonJSPlugin,
  FollowCamera,
  UniversalCamera,
} from "@babylonjs/core";

import Character from "./CharacterClass";
// import { Viewport } from "@babylonjs/core/Maths/math.viewport";
import "@babylonjs/loaders/glTF";
import * as CANNON from "cannon-es";
window.CANNON = CANNON;

export class BasicScene {
  // createFollowCamera(scene: Scene, heroDude: any): any {
  //   throw new Error("Method not implemented.");
  // }
  scene: Scene;
  engine: Engine;
  Game = { scenes: [], activeScene: 0 };
  isWPressed = false;
  isSPressed = false;
  isAPressed = false;
  isDPressed = false;
  isBPressed = false;
  isRPressed = false;
  camera: any;
  character: any;
  player: any;

  constructor(private canvas: HTMLCanvasElement) {
    this.engine = new Engine(this.canvas, true);
    this.scene = this.CreateScene();
    // this.modifySettings(this.scene);
    // this.loadCrosshair();
    this.engine.runRenderLoop(() => {
      this.scene.render();
    });
  }

  CreateScene(): Scene {
    const scene = new Scene(this.engine);
    // const gravityVector = new Vector3(0, -9.81, 0);
    // const character = this.createCharacter(scene);
    // const physicsPlugin = new CannonJSPlugin();
    // this.configureAssetsManager(scene);
    // scene.enablePhysics(gravityVector, physicsPlugin);
    this.camera = this.createCamera(scene);
    const ground = this.CreateGround(scene);
    const hemiLight = new HemisphericLight(
      "hemiLight",
      new Vector3(0, 1, 0),
      this.scene
    );
    hemiLight.intensity = 1;
    // this.addEventListeners();
    return scene;
  }

  createCamera = (scene: Scene): UniversalCamera => {
    console.log("🚀 ~ file: BasicScene.ts ~ line 79 ~ BasicScene ~ createCamera ~ this.scene", this.scene)
    const camera = new UniversalCamera("camera", new Vector3(0, 5, 5), scene);
    camera.applyGravity = true;
    camera.ellipsoid = new Vector3(.4, .8, .4);
    camera.checkCollisions = true;
    camera.attachControl(this.canvas, true); 
    console.log("🚀 ~ file: BasicScene.ts ~ line 82 ~ BasicScene ~ this.canvas", this.canvas)
    // camera.attachControl(this.canvas, true);
    // camera.inputs.attachInput(camera.inputs.attached.mouse);

    camera.speed = 0.25;
    return camera;
  }

  // createPlayer(): Mesh | any {
  //   const player: Mesh | any = MeshBuilder.CreateBox(
  //     "player",
  //     { height: 4, depth: 6, width: 6 },
  //     this.scene
  //   );
  //   const playerMaterial = new StandardMaterial("playerMaterial", this.scene);
  //   playerMaterial.diffuseColor = Color3.Red();
  //   playerMaterial.emissiveColor = Color3.Blue();
  //   player.material = playerMaterial;
  //   player.position.y += 2;
  //   player.speed = 1;
  //   player.frontVector = new Vector3(0, 0, 1);
  //   player.canFireCannonBalls = true;
  //   player.canFireLaser = true;
  //   player.move = (scene: Scene | any) => {
  //     scene.activeCamera = scene.cameras[0];
  //     // let yMovement = 0;
  //     if (player.position.y > 2) {
  //       player.moveWithCollisions(new Vector3(0, -2, 0));
  //     }

  //     if (this.isWPressed) {
  //       player.moveWithCollisions(
  //         player.frontVector.multiplyByFloats(
  //           player.speed,
  //           player.speed,
  //           player.speed
  //         )
  //       );
  //     }
  //     if (this.isSPressed) {
  //       player.moveWithCollisions(
  //         player.frontVector.multiplyByFloats(
  //           -1 * player.speed,
  //           -1 * player.speed,
  //           -1 * player.speed
  //         )
  //       );
  //     }
  //     if (this.isAPressed) {
  //       player.rotation.y -= 0.1;
  //       player.frontVector = new Vector3(
  //         Math.sin(player.rotation.y),
  //         0,
  //         Math.cos(player.rotation.y)
  //       );
  //     }
  //     if (this.isDPressed) {
  //       player.rotation.y += 0.1;
  //       player.frontVector = new Vector3(
  //         Math.sin(player.rotation.y),
  //         0,
  //         Math.cos(player.rotation.y)
  //       );
  //     }
  //   };
  //   player.fireLaserBeams = (scene: Scene | any) => {
  //     if (!this.isRPressed) return;
  //     if (!player.canFireLaser) return;
  //     player.canFireLaser = false;

  //     setTimeout(function () {
  //       player.canFireLaser = true;
  //     }, 500);

  //     scene.assets["laserSound"].play();
  //     const origin = player.position;
  //     const direction = new Vector3(
  //       player.frontVector.x,
  //       player.frontVector.y + 0.1,
  //       player.frontVector.z
  //     );

  //     const ray = new Ray(origin, direction, 1000);
  //     const rayHelper = new RayHelper(ray);
  //     rayHelper.show(scene, new Color3(0xffffff));

  //     setTimeout(() => {
  //       // rayHelper.hide(ray)
  //       rayHelper.hide();
  //     }, 200);

  //     const pickInfos = scene.multiPickWithRay(ray, function (mesh) {
  //       if (mesh.name == "heroTank") return false;
  //       return true;
  //     });

  //     for (let i = 0; i < pickInfos.length; i++) {
  //       const pickInfo = pickInfos[i];
  //       if (pickInfo.pickedMesh) {
  //         if (pickInfo.pickedMesh.name.startsWith("bounder")) {
  //           pickInfo.pickedMesh.dudeMesh.Dude.decreaseHealth(
  //             pickInfo.pickedPoint
  //           );
  //         } else if (pickInfo.pickedMesh.name.startsWith("clone")) {
  //           pickInfo.pickedMesh.parent.Dude.decreaseHealth(
  //             pickInfo.pickedPoint
  //           );
  //         }
  //       }
  //     }
  //   };

  //   return player;
  // }

  // loadCrosshair(): void {
  //   const impact = CreateBox("impact", { size: 0.01 }, this.scene);
  //   impact.parent = this.scene.cameras[0];
  //   this.scene.cameras[0].minZ = 0.1;
  //   impact.position.z += 0.2;
  //   const material = new StandardMaterial("impact", this.scene);
  //   material.diffuseTexture = new Texture("images/gunaims.png", this.scene);
  //   material.diffuseTexture.hasAlpha = true;
  //   impact.isPickable = false;
  //   impact.material = material;
  // }

  CreateGround = (scene: Scene): GroundMesh => {
    const ground = CreateGroundFromHeightMap(
      "ground",
      "environment/hmap1.png",
      {
        // width: 2000,
        // height: 2000,
        // subdivisions: 20,
        // minHeight: 0,
        // maxHeight: 1000,
        // onReady: this.onGroundReady,
      },
      scene
    );
    return ground;
  };
  onGroundReady = (ground: GroundMesh) => {
    // const groundMaterial = new StandardMaterial("groundMaterial", this.scene);
    // groundMaterial.diffuseTexture = new Texture(
    //   "environment/grass.jpg",
    //   this.scene
    // );
    // ground.material = groundMaterial;
    // ground.checkCollisions = true;
    // console.log(ground);
    //   const groundPhysicsImposter = new PhysicsImpostor(
    //     ground,
    //     PhysicsImpostor.HeightmapImpostor,
    //     { mass: 0 },
    //     this.scene
    //   );
    //   ground.physicsImpostor = groundPhysicsImposter;
  };

  createFreeCamera(initialPosition: any) {
      // const camera = new FreeCamera("freeCamera", initialPosition, this.scene);
      // camera.attachControl(this.canvas);
      // camera.checkCollisions = true;
      // camera.applyGravity = true;
      // camera.ellipsoid = new Vector3(0.1, 0.1, 0.1);
      // camera.keysUp.push("w".charCodeAt(0));
      // camera.keysUp.push("W".charCodeAt(0));
      // camera.keysDown.push("s".charCodeAt(0));
      // camera.keysDown.push("S".charCodeAt(0));
      // camera.keysRight.push("d".charCodeAt(0));
      // camera.keysRight.push("D".charCodeAt(0));
      // camera.keysLeft.push("a".charCodeAt(0));
      // camera.keysLeft.push("A".charCodeAt(0));

      // return camera;
  }

  createCharacter = (scene: Scene): Character => {
    const assetsManager = new AssetsManager(scene);
    const meshTask = assetsManager.addMeshTask(
      "DudeTask",
      "him",
      "models/Dude/",
      "Dude.babylon"
    );
    let hero!: Character;
    meshTask.onSuccess = (task: any) => {
      console.log("succeeded");
      this.onCharacterImported(
        task.loadedMeshes,
        task.loadedParticleSystems,
        task.loadedSkeletons
      );
      hero = new Character(task.loadedMeshes, 2, -1, this.scene, 0.2);
    };
    return hero;
  };

  onCharacterImported(newMeshes: any, particleSystems: any, skeletons: any) {
    console.log("🚀 skeletons", skeletons)
    console.log("🚀 particleSystems", particleSystems)
    newMeshes[0].position = new Vector3(0, 0, 5); // The original dude
    newMeshes[0].name = "heroDude";
    const heroDude = newMeshes[0];
    console.log("🚀  heroDude", heroDude)
  }

  configureAssetsManager = (scene: Scene) => {
    const assetsManager = new AssetsManager(scene);
    assetsManager.onProgress = (
      remainingCount,
      totalCount,
      lastFinishedTask
    ) => {
      console.log("🚀 lastFinishedTask", lastFinishedTask);
      this.engine.loadingUIText =
        "We are loading the scene. " +
        remainingCount +
        " out of " +
        totalCount +
        " items still need to be loaded.";
    };

    assetsManager.onFinish = (tasks) => {
      console.log("🚀 ~ tasks", tasks);
    };

    return assetsManager;
  };

  modifySettings(scene: Scene) {
    // scene.onPointerDown = () => {
    //   console.log(scene);
    // };
  }

  addEventListeners = () => {
    document.addEventListener("keydown",(event) => {
      if (event.key == "w" || event.key == "W") {
        this.isWPressed = true;
      }
      if (event.key == "s" || event.key == "S") {
        this.isSPressed = true;
      }
      if (event.key == "a" || event.key == "A") {
        this.isAPressed = true;
      }
      if (event.key == "d" || event.key == "D") {
        this.isDPressed = true;
      }
      if (event.key == "b" || event.key == "B") {
        this.isBPressed = true;
      }
      if (event.key == "r" || event.key == "R") {
        this.isRPressed = true;
      }
    });
    document.addEventListener("keyup", (event) => {
      if (event.key == "w" || event.key == "W") {
        this.isWPressed = false;
      }
      if (event.key == "s" || event.key == "S") {
        this.isSPressed = false;
      }
      if (event.key == "a" || event.key == "A") {
        this.isAPressed = false;
      }
      if (event.key == "d" || event.key == "D") {
        this.isDPressed = false;
      }
      if (event.key == "b" || event.key == "B") {
        this.isBPressed = false;
      }
      if (event.key == "r" || event.key == "R") {
        this.isRPressed = false;
      }
    });
  }
}
