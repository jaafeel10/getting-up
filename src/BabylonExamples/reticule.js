import {
UtilityLayerRenderer,
DynamicTexture,
StandardMaterial,
MeshBuilder
} from 'babylonjs';

function createOutline(ctx, w) {
    let c = 2

    ctx.moveTo(c, w * 0.25)
    ctx.lineTo(c, c)
    ctx.lineTo(w * 0.25, c)

    ctx.moveTo(w * 0.75, c)
    ctx.lineTo(w - c, c)
    ctx.lineTo(w - c, w * 0.25)

    ctx.moveTo(w - c, w * 0.75)
    ctx.lineTo(w - c, w - c)
    ctx.lineTo(w * 0.75, w - c)

    ctx.moveTo(w * 0.25, w - c)
    ctx.lineTo(c, w - c)
    ctx.lineTo(c, w * 0.75)

    ctx.lineWidth = 1.5
    ctx.strokeStyle = 'rgba(128, 128, 128, 0.5)'
    ctx.stroke()
}

function createNavigate(ctx, w, texture) {
    ctx.fillStyle = 'transparent'
    ctx.clearRect(0, 0, w, w)
    createOutline(ctx, w)

    ctx.strokeStyle = 'rgba(48, 48, 48, 0.9)'
    ctx.lineWidth = 3.5
    ctx.moveTo(w * 0.5, w * 0.25)
    ctx.lineTo(w * 0.5, w * 0.75)

    ctx.moveTo(w * 0.25, w * 0.5)
    ctx.lineTo(w * 0.75, w * 0.5)
    ctx.stroke()
    ctx.beginPath()

    texture.update()
}

function addCrosshair(scene) {
    var utilLayer = new UtilityLayerRenderer(scene);
    let w = 128
    let texture = new DynamicTexture('reticule', w, scene, false)
    texture.hasAlpha = true

    let ctx = texture.getContext()
    let reticule;
    
    console.log("🚀 ~ reticule", reticule);
    createNavigate(ctx, w, texture);

    let material = new StandardMaterial('reticule', scene)
    material.diffuseTexture = texture
    material.opacityTexture = texture
    material.emissiveColor.set(1, 1, 1)
    material.disableLighting = true

    let plane = MeshBuilder.CreatePlane('reticule', { size: 0.04 }, utilLayer.utilityLayerScene)
    plane.material = material
    plane.position.set(0, 0, 1.1)
    plane.isPickable = false
    plane.rotation.z = Math.PI / 4

    reticule = plane
    reticule.parent = scene.cameras[0];
    return reticule
}

export default {
    addCrosshair
}