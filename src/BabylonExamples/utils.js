import {
    DynamicTexture,
    StandardMaterial,
    MeshBuilder,
    HemisphericLight,
    PointLight,
    UtilityLayerRenderer,
    Engine,
    Scene,
    Color3,
    Vector3,
    SceneOptimizerOptions,
    SceneOptimizer,
    HardwareScalingOptimization,
    CannonJSPlugin,
    UniversalCamera,
    PhysicsImpostor,
    Mesh } from 'babylonjs';

import CANNON from 'cannon';

window.CANNON = CANNON;

var assumedFramesPerSecond = 60;
var earthGravity = -9.81;

function getEngine(canvas) {
    return new Engine(canvas, true);
}

function getScene(engine) {
    return new Scene(engine);
}

function getFps() {
    return document.getElementById("fps");
}

function getAmbientColor() {
    return new Color3(1, 1, 1)
}

function getSceneOptimizers(scene) {
    var optimizerOptions = new SceneOptimizerOptions();

    optimizerOptions.addOptimization(new HardwareScalingOptimization(0, 1));
    
    var optimizer = new SceneOptimizer(scene, optimizerOptions);

    return  optimizer 
}

function addScenePhysics(scene) {
    scene.collisionsEnabled = true;
    scene.enablePhysics(new Vector3(0, earthGravity / assumedFramesPerSecond, 0), new CannonJSPlugin(true, 10, CANNON));
}

function getSceneCamera(scene) {
    var camera = new UniversalCamera("UniversalCamera", new Vector3(0, 2, -25), scene);
    
    camera.setTarget(Vector3.Zero());
    
    camera.applyGravity = true;
    
    camera.ellipsoid = new Vector3(.4, .8, .4);
    
    camera.checkCollisions = true;
    
    camera.attachControl( true);

    return camera
}

function getHeroMesh(scene) {
    var hero = MeshBuilder.CreateBox('hero', {size: 2.0}, scene, false, Mesh.FRONTSIDE);
    
    hero.position.x = 0.0;
    hero.position.y = 1.0;
    hero.position.z = 0.0;
    hero.physicsImpostor = new PhysicsImpostor(hero, PhysicsImpostor.BoxImpostor, { mass: 1, restitution: 0.0, friction: 0.1 }, scene);
    hero.isPickable = false;

    return hero;
}

function getGroundMesh(scene) {
    var ground = MeshBuilder.CreateGround("ground", { width: 200, height: 200, subdivsions: 4 }, scene);
    
    ground.material = new StandardMaterial("ground", scene);
    ground.position.y = -1;
    ground.checkCollisions = true;
    ground.physicsImpostor = new PhysicsImpostor(ground, PhysicsImpostor.BoxImpostor, { mass: 0, restitution: 0 }, scene);
    ground.isPickable = false;

    return ground;
}

function getWallMesh(scene) {
    var wall = MeshBuilder.CreateBox("wall", {
        height: 8,
        width: 15,
        depth: 1
    }, scene);
    
    wall.isPickable = true;
    wall.position.x = scene.cameras[0].position.x;
    wall.position.y = scene.cameras[0].position.y;
    wall.position.z = scene.cameras[0].position.z;

    wall.physicsImpostor = new PhysicsImpostor(wall, PhysicsImpostor.BoxImpostor, { mass: 0, restitution: 0 }, scene);
    wall.checkCollisions = true;
    
    
    return wall;
}

function getBorderBox(scene) {

    var border0 = Mesh.CreateBox("border0", 1, scene);
    border0.scaling = new Vector3(5, 100, 200);
    border0.position.x = -100.0;
    border0.checkCollisions = true;
    border0.isVisible = false;

    var border1 = Mesh.CreateBox("border1", 1, scene);
    border1.scaling = new Vector3(5, 100, 200);
    border1.position.x = 100.0;
    border1.checkCollisions = true;
    border1.isVisible = false;

    var border2 = Mesh.CreateBox("border2", 1, scene);
    border2.scaling = new Vector3(200, 100, 5);
    border2.position.z = 100.0;
    border2.checkCollisions = true;
    border2.isVisible = false;

    var border3 = Mesh.CreateBox("border3", 1, scene);
    border3.scaling = new Vector3(200, 100, 5);
    border3.position.z = -100.0;
    border3.checkCollisions = true;
    border3.isVisible = false;

    border0.physicsImpostor = new PhysicsImpostor(border0, PhysicsImpostor.BoxImpostor, { mass: 0 }, scene);
    border1.physicsImpostor = new PhysicsImpostor(border1, PhysicsImpostor.BoxImpostor, { mass: 0 }, scene);
    border2.physicsImpostor = new PhysicsImpostor(border2, PhysicsImpostor.BoxImpostor, { mass: 0 }, scene);
    border3.physicsImpostor = new PhysicsImpostor(border3, PhysicsImpostor.BoxImpostor, { mass: 0 }, scene);
}

function addLights(scene) {
    var light1 = new HemisphericLight("light1", new Vector3(1, 1, 0), scene);
    var light2 = new PointLight("light2", new Vector3(60, 60, 0), scene);
    
    light1.intensity = 0.5;
    light2.intensity = .5;
    
    return {
        light1,
        light2
    }
}

export default {
    getEngine,
    getScene,
    getFps,
    getAmbientColor,
    getSceneOptimizers,
    addScenePhysics,
    getSceneCamera,
    getHeroMesh,
    getGroundMesh,
    getWallMesh,
    getBorderBox,
    addLights
}