import * as BABYLON from 'babylonjs';
import { Vector3 } from 'babylonjs';
var createPS = function(scene, raycast, path) {
console.log("🚀 ~ path", path.length);
console.log("🚀 ~ scene", scene);
console.log("🚀 ~ raycast", raycast);

	// var particleNb = 1;
		
     var cylinder = BABYLON.MeshBuilder.CreateCylinder("cylinder", {
        height: 0.01,
        diameter: .1,
        faceColors: [new BABYLON.Color4(1, 0, 0, 1), new BABYLON.Color4(1,0,0, 1), new BABYLON.Color4(1,0,0, 1)]
    }, scene)

    cylinder.position = new Vector3(raycast.pickedPoint.x, raycast.pickedPoint.y, raycast.pickedPoint.z)

    // var SPS = new BABYLON.SolidParticleSystem('SPS', scene,  {particleIntersection: true});
	// SPS.addShape(cylinder, particleNb);
	// cylinder.dispose();
    // var colorArr = [];
    // colorArr.push( BABYLON.Color4(1,0,0,1))
    // var mesh = SPS.buildMesh();
	// mesh.position.x = raycast.pickedPoint.x
	// mesh.position.y = raycast.pickedPoint.y
	// mesh.position.z = raycast.pickedPoint.z
	// //q is any other particle than the current particle
    // mesh.rotate(new BABYLON.Vector3(1, 0, 0), Math.PI /2) ;
   
    // mesh.freezeWorldMatrix();

	//CURRENT POSITION
	var dx = 0; //difference of centres along x axis
	var dy = 0; //difference of centres along y axis
	var dz = 0; //difference of centres along z axis
	var dx2 = 0; //difference squared along x axis
	var dy2 = 0; //difference squared along y axis
	var dz2 = 0; //difference squared along z axis
	var dl = 0; // length between centres
	var dl2 = 0; //length squared
	var nx = 0; // component of unit vector along line joining centres r
	var ny = 0; // component of unit vector along line joining centres r
	var nz = 0; // component of unit vector along line joining centres r
	var pdotr = 0; //particle velocity dot (nx, ny, nz)
	var qdotr = 0; // q velocity dot r (nx, ny, nz)
	
	//CURRENT VELOCITIES
	var vx = 0; //difference of velocities along x axis
	var vy = 0; //difference of velocities along y axis
	var vz = 0; //difference of velocities along z axis
	var vx2 = 0; //difference squared along x axis
	var vy2 = 0; //difference squared along y axis
	var vz2 = 0; //difference squared along z axis
	var vl = 0; // sqrt diff dot diff
	var vl2 = 0; // diff dot diff
	var vdotd = 0; // diff of velocities dot diff of positions
	var vdotn;
	var sq = 0; // quadratic b^2 - 4ac
	var sqroot = 0; // root of b^2 - 4ac
	var t = 0; //time to collision
	
	// SPS initialization 
	// SPS.initParticles = function() {
	// 	for (var p = 0; p < SPS.nbParticles; p++) {
    
	// 		// var x = scene.activeCamera.position.x;
	// 		// var y = scene.activeCamera.position.y;
	// 		// var z = scene.activeCamera.position.z;
	// 		// SPS.particles[p].direction = new BABYLON.Vector3(raycast.pickedPoint.x, raycast.pickedPoint.y, raycast.pickedPoint.z);
		
	// 		// SPS.particles[p].position = new BABYLON.Vector3(x, y, z);
	// 		// SPS.particles[p].velocity = new BABYLON.Vector3(particleSpeedX * SPS.particles[p].direction.x, particleSpeedY * SPS.particles[p].direction.y, particleSpeedZ * SPS.particles[p].direction.z);
	// 		// SPS.particles[p].color = new BABYLON.Color4(0, Math.random(), Math.random(), 1);
	// 	}
	// 	// SPS.particles[0].color = new BABYLON.Color4(1, 0, 0, 1); //allows on particle to be followed
		
		
	// };
	
	var epsilon = 0.00001; // 
	
	// SPS.updateParticle = function(particle) { 
	// 	for (var p = particle.idx + 1; p < SPS.nbParticles; p++) {	

	// 		var q = SPS.particles[p];
	// 		dx = particle.position.x - q.position.x;
	// 		dy = particle.position.y - q.position.y;
	// 		dz = particle.position.z - q.position.z;
	// 		dx2 = dx * dx;
	// 		dy2 = dy * dy;
	// 		dz2 = dz * dz;
	// 		dl2 = dx2 + dy2 + dz2;
		
	// 		vx = particle.velocity.x - q.velocity.x;
	// 		vy = particle.velocity.y - q.velocity.y;
	// 		vz = particle.velocity.z - q.velocity.z;
	// 		vx2 = vx * vx;
	// 		vy2 = vy * vy;
	// 		vz2 = vz * vz;
	// 		vl2 = vx2 + vy2 + vz2;
		
	// 		vdotd = dx * vx + dy * vy + dz * vz;
		
	// 		sq = 4 * vdotd * vdotd - 4 * vl2 * (dl2 - 4 * 1 * 1);
		
	// 		if(vdotd < 0 && sq >0) {
	// 			sqroot = Math.sqrt(sq);
	// 			t = (-2 * vdotd - sqroot)/(2 * vl2);
	// 			if(0 < t && t <= 1 ) {

	// 				//new velocity
	// 				dx += vx;
	// 				dy += vy;
	// 				dz += vz;
	// 				dx2 = dx * dx;
	// 				dy2 = dy * dy;
	// 				dz2 = dz * dz;
	// 				dl2 = dx2 + dy2 + dz2;
	// 				if(dl2 == 0) {
	// 					dl2 = 1;
	// 				}
	// 				dl = Math.sqrt(dl2);
	// 				nx = dx/dl;
	// 				ny = dy/dl;
	// 				nz = dz/dl;
				
	// 				vdotn = nx * vx + ny * vy + nz * vz;
				
	// 				particle.velocity.x -= vdotn * nx;
	// 				particle.velocity.y -= vdotn * ny;
	// 				particle.velocity.z -= vdotn * nz;
	// 				q.velocity.x += vdotn * nx;
	// 				q.velocity.y += vdotn * ny;
	// 				q.velocity.z += vdotn * nz;
				
	// 				//position correction
	// 				particle.position.x += vdotn * nx * t;
	// 				particle.position.y += vdotn * ny * t;
	// 				particle.position.z += vdotn * nz * t;
	// 				q.position.x -= vdotn * nx * t;
	// 				q.position.y -= vdotn * ny * t;
	// 				q.position.z -= vdotn * nz * t;
	// 			}
	// 		}
	// 	}
			
		
	// 	var nextx = particle.position.x + particle.velocity.x;
    //     if(nextx - raycast.pickedPoint.x <= (1 + epsilon) * 1 && particle.velocity.x < 0) {
	// 		particle.position.x = raycast.pickedPoint.x;
	// 		particle.position.y = raycast.pickedPoint.y;
	// 		particle.position.z = raycast.pickedPoint.z;
	// 	} 
		
	// 	particle.position.x += particle.velocity.x;
	// 	particle.position.y += particle.velocity.y;
	// 	particle.position.z += particle.velocity.z;
    //     // particle.rotation = new BABYLON.Vector3(1, 0, 0)
	// 	particle.color = new BABYLON.Color4(1,0,0,1)
    //     SPS.computeParticleColor = false;
    //     SPS.computeParticleTexture = false;
	// }
		
	// // SPS.initParticles();
	// scene.registerAfterRender(function() {	
	// 	SPS.setParticles();
     
	// });

	return scene;

    function getDiameter(raycast) {
        if(raycast) {
        // var k = 12.8;
        // var distance = raycast.distance;
        // var thickness = k /distance * distance;
        // let newWidth;

       }
        
    //    console.log("🚀 ~ width", width);
    //    return width;
    }
};

export default createPS;