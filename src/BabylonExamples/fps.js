// You have to create a function called createScene. This function must return a BABYLON.Scene object
// You can reference the following variables: scene, canvas
// You must at least define a camera
// / <reference path="./babylon.max.js" />


import * as BABYLON from 'babylonjs';
import { Matrix, MeshBuilder, RayHelper, Scene, Engine } from 'babylonjs';
import utils from './utils';
import Reticule from './reticule';
import controls from './controls';

var isDragging = false;

var createScene = function(canvas) {

    var divFps = utils.getFps();
    
    canvas.requestPointerLock();
    
    var engine = utils.getEngine(canvas);

    var scene = new Scene(engine);

    scene.ambientColor = utils.getAmbientColor();

    var optimizerOptions = utils.getSceneOptimizers(scene).optimizerOptions;

    utils.addScenePhysics(scene);

    var camera = utils.getSceneCamera();
    
    var cylinder = MeshBuilder.CreateCylinder("cylinder", {
        height: 0.0001,
        diameter: 1,
        faceColors: [new BABYLON.Color4(1, 0, 0, 1), new BABYLON.Color4(1,0,0, 1), new BABYLON.Color4(1,0,0, 1)]
    }, scene);

    cylinder.isVisible = false;

    cylinder.emissiveColor = new BABYLON.Color3(1, 0, 0);

    var hero = utils.getHeroMesh();
    
    Reticule.addCrosshair(scene);

    document.addEventListener('keydown', controls.onKeyDown, false);
    document.addEventListener('keyup', controls.onKeyUp, false);

    scene.registerBeforeRender(function () {
        controls.setWalkingBehavior(hero, camera);
    });
 
    engine.runRenderLoop(() => { 
        scene.render();
        divFps.innerHTML = engine.getFps().toFixed() + " fps";
    });

    var ground = utils.getGroundMesh(scene);

    var wall = utils.getWallMesh(scene);

    var borderbox = utils.getBorderBox(scene);

    //We start without being locked.
	var isLocked = false;

	// Event listener when the pointerlock is updated (or removed by pressing ESC for example).
	var pointerlockchange = function () {
		var controlEnabled = document.mozPointerLockElement || document.webkitPointerLockElement || document.msPointerLockElement || document.pointerLockElement || null;
		if (!controlEnabled) {
			isLocked = false;
		} else {
			isLocked = true;
		}
	};
	
	// Attach events to the document
	document.addEventListener("pointerlockchange", pointerlockchange, false);
	document.addEventListener("mspointerlockchange", pointerlockchange, false);
	document.addEventListener("mozpointerlockchange", pointerlockchange, false);
	document.addEventListener("webkitpointerlockchange", pointerlockchange, false);

    utils.addLights(scene);

    scene.onPointerUp = () => {
        isDragging = false;
    }

    scene.onPointerDown = () => {
        isDragging = true;
        if (!isLocked) {
			canvas.requestPointerLock = canvas.requestPointerLock || canvas.msRequestPointerLock || canvas.mozRequestPointerLock || canvas.webkitRequestPointerLock;
			if (canvas.requestPointerLock) {
				canvas.requestPointerLock();
			}
		}
        const ray = scene.createPickingRay(scene.pointerX, scene.pointerY, Matrix.Identity(), camera)
        const raycastHit = scene.pickWithRay(ray);        
        if (raycastHit.hit) particlize(raycastHit);
        
    }

    scene.onPointerMove = () => {    
        if (isDragging) {
            const ray = scene.createPickingRay(scene.pointerX, scene.pointerY, Matrix.Identity(), camera)
            const raycastHit = scene.pickWithRay(ray);        
            if (raycastHit.hit) particlize(raycastHit);
        }
    }

    function particlize(raycastHit) {
        var instance = cylinder.createInstance("sps_");
        if (raycastHit.hit) {
            instance.position.x = raycastHit.pickedPoint.x
            instance.position.y = raycastHit.pickedPoint.y
            instance.position.z = raycastHit.pickedPoint.z
            instance.rotate(new BABYLON.Vector3(1, 0, 0), Math.PI /2)
        }
    }

    return scene;
};

export default createScene;
